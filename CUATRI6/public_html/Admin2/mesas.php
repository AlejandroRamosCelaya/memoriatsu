<?php
session_start();
	include "../conexion.php";
	if(isset($_SESSION['segundo'])){
?>
<html lang="es">
<head>
    <meta charset="utf-8"/>
    <title></title>
    <link rel="stylesheet" type="text/css" href="../css/style.css">

</head>
<body>

	<?php 
		include "menu.php";
	?>

	<div style="float:right;margin-right:185px;">
		<a href="#ventana1" class="btn btn-default active" data-toggle="modal">Crear orden</a>
		<!-- Inicia Vetana Modal -->
		<div class="modal fade" id="ventana1">
			<div class="modal-dialog">
				<div class="modal-content">
					<!-- Titulo -->
					<div class="modal-header">
						<button tyle="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
						<h2 class="modal-title">Crear orden</h2>
					</div>
					
					<!-- Contenido -->
					<div class="modal-body">
						<form method="POST" action="pedido/crear.php" enctype="multipart/form-data" id="Agregando">
							<?php
							/* COMIENZA LA ASIGNACION DE NUMERO DE ORDEN */	
								$numeroventa=0;
								$re = $conexion->query("SELECT MAX(numeroOrden) AS numeroOrden FROM Orden") or die(mysql_error());
								while ($f= mysqli_fetch_assoc($re)) {
											$numeroventa= $f['numeroOrden'];
								}
								if($numeroventa==0){
									$numeroventa=1;
								}else{
									$numeroventa=$numeroventa+1;
								}
							/* TERMINA LA ASIGNACION DE NUMERO DE ORDEN */

							/* COMIENZA LA BUSQUEDA DE PLATILLOS */
								$pl = $conexion->query("select * from platillos ") or die(mysql_error());
							/* TERMINA LA BUSQUEDA DE PLATILLOS */

							/* COMIENZA LA BUSQUEDA DE PLATILLOS */
								$ms= $_SESSION['segundo']['Usuario'];
							/* TERMINA LA BUSQUEDA DE PLATILLOS */

							?>
							<div class="col-xs-8">
								<label>Numero de orden:</label><br>
								<label><?php echo $numeroventa; ?></label>
							</div>
							<br><br><br>
							
							<div class="col-xs-6">
								<label>Producto:</label><br>
								<select type="text" name="producto" class="form-control" required>
									<?php
									while ($f= mysqli_fetch_assoc($pl)) {
										echo '<option>'.$f['nombre'].'</option>';
									}
									?>
								</select>
							</div>
							<br><br><br><br>

							<div class="col-xs-8">
								<label>Cantidad:</label>
								<input type="number" name="cantidad" class="form-control">
							</div>
							<br><br><br><br>
							
							<div class="col-xs-6">
								<label>Nombre mesero:</label><br>
								<input type="" name="" value="<?php echo $ms; ?>" disabled>
							</div>
							<br><br><br><br>

					</div>

					<!-- Pie -->
					<div class="modal-footer">
						<button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
						<button type="submit" class="btn btn-success">Crear</button>
						</form>
					</div>
				</div>
			</div>			
		</div>
		<!-- Termina Vetana Modal -->

	</div>
	<br><br>

	<div style="padding: 2%;" class="container">
		<table class="table table-striped">
			<thead>
				<tr>
					<th style="color:#0070ff;">Numero de orden</th>
					<th></th>
				</tr>
			</thead>
			<?php
				$consulta= "select DISTINCT numeroOrden from Orden where Status = 1";
				$re= $conexion->query($consulta);
				//$resultado=$mysqli->query($query);
				while($row=$re ->fetch_assoc()){
					?>
						<tbody>
							<tr>
								<td style="color:red;">
									<?php echo ''.$row['numeroOrden'].''; ?>
								</td>
							
								<td style="padding: .5%;">
									<form method="POST" action="orden.php" enctype="multipart/form-data">					
										<input type="hidden" name="orden" value="<?php echo ''.$row['numeroOrden'].''; ?>">
										<button class="btn btn-success">Acceder</button>
									</form>
								</td>
							</tr>
						</tbody>
			
					<?php
				}
			?>
		</table>
	</div>
	
</body>
</html>
<?php 
}else{
		header("Location: ../login.php?Error=Acceso denegado");
}
?>