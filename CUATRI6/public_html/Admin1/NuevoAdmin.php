<?php
session_start();
	include "../conexion.php";
	if(isset($_SESSION['primero'])){

?>
<html lang="es">
<head>
    <meta charset="utf-8"/>
    <title></title>
    <link rel="stylesheet" type="text/css" href="../css/style.css">
</head>
<body>

	<?php 
		include "menu.php";
	?>
        
 

	<section>

	<div id="Adminuevo" style="opacity:0.9; margin-top:50px;">
		<center><h1>Agregar Administrador</h1></center>
		<form action="admins/altaadmi.php" method = "post" id="Agregando">
			<div class="col-xs-6">
				<label>Usuario:</label><br>
				<input type="text" name="UsuarioN" class="form-control"
				required placeholder="Ingrese su nombre." style="background:#BBBADB;"/>
			</div>
			<div class="col-xs-6">
				<label>Password:</label><br>
				<input type="password" name="PasswordN" class="form-control"
				required placeholder="Ingrese sus apellidos." style="background:#BBBADB;"/>
			</div>
			<div class="col-xs-6">
				<label>Nombre:</label><br>
				<input type="text" name="NombreN" class="form-control"
				required placeholder="Ingrese su nombre." style="background:#BBBADB;"/>
			</div>
			<div class="col-xs-6">
				<label>Apellidos:</label><br>
				<input type="text" name="ApellidosN" class="form-control"
				required placeholder="Ingrese sus apellidos." style="background:#BBBADB;"/>
			</div>
			<div class="col-xs-6">
				<label>Email:</label><br>
				<input type="email" name="EmailN" class="form-control"
				required placeholder="Ingrese su email." style="background:#BBBADB;"/>
			</div>
			<div class="col-xs-6">
				<label>Tipo de usuario:</label><br>
				<select type="text" name="TipoN" class="form-control" required>
					<?php
					$consulta= "select * from TipoUsuaio where Status = 1 ";
					$re= $conexion->query($consulta);
					while($row=$re ->fetch_assoc()){

						echo '<option>'.$row['tipoNom'].'</option>';
					}
					?>
				</select>
			</div>			
			<button type="submit" class="btn btn-success">Acceder</button>
		</form>
	</div>	
	</section>
</body>
</html>
<?php
}else{
	header("Location: ../login.php?Error=Acceso denegado");
}
?>