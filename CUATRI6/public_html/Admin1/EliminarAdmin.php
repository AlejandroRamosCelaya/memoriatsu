<?php
session_start();
	include "../conexion.php";
	if(isset($_SESSION['primero'])){
?>
<html lang="es">
<head>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width,initial-scale=1.0">
    <title></title>
    <link rel="stylesheet" type="text/css" href="../css/style.css">

</head>
<body>

	<?php 
		include "menu.php";
	?>
 
	
	<div style="float:left;margin-left:45px;padding:70px;">
		<a href="#ventana1" class="btn btn-primary" data-toggle="modal">Agregar administrador</a>
		<!-- Inicia Vetana Modal -->
		<div class="modal fade" id="ventana1">
			<div class="modal-dialog">
				<div class="modal-content">
					<!-- Titulo -->
					<div class="modal-header">
						<button tyle="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
						<h2 class="modal-title">Agregar Administrador</h2>
					</div>
					
					<!-- Contenido -->
					<div class="modal-body">
						<form action="admins/altaadmi.php" method = "post" id="Agregando">
							<div class="col-xs-6">
								<label>Usuario:</label><br>
								<input type="text" name="UsuarioN" class="form-control"
								required placeholder="Ingrese su nombre."/>
							</div>
							<div class="col-xs-6">
								<label>Password:</label><br>
								<input type="password" name="PasswordN" class="form-control"
								required placeholder="Ingrese sus apellidos."/>
							</div>
							<div class="col-xs-6">
								<label>Nombre:</label><br>
								<input type="text" name="NombreN" class="form-control"
								required placeholder="Ingrese su nombre."/>
							</div>
							<div class="col-xs-6">
								<label>Apellidos:</label><br>
								<input type="text" name="ApellidosN" class="form-control"
								required placeholder="Ingrese sus apellidos."/>
							</div>
							<div class="col-xs-6">
								<label>Email:</label><br>
								<input type="email" name="EmailN" class="form-control"
								required placeholder="Ingrese su email."/>
							</div>
							<div class="col-xs-6">
								<label>Tipo de usuario:</label><br>
								<select type="text" name="TipoN" class="form-control" required>
									<?php
									$consulta= "select * from TipoUsuaio where Status = 1 ";
									$re= $conexion->query($consulta);
									while($row=$re ->fetch_assoc()){

										echo '<option>'.$row['tipoNom'].'</option>';
									}
									?>
								</select>
							</div>
							<br><br><br><br><br><br><br><br><br>
					</div>
					<div class="modal-footer">
							<button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
							<button type="submit" class="btn btn-success">Guardar</button>
						</form>
					</div>
				</div>
			</div>			
		</div>
		<!-- Termina Vetana Modal -->
	</div>

	<div style="width: 30%; background:#CACACA; border-radius:20px; 
	margin-top:40px; margin-right:40px; padding:10px; float:right;">
		<form action="admins/bajaadmi.php" method="POST">
			<div class="col-xs-6">
				<label>Usuario:</label>
				<input type="text" name="AdmiUser" class="form-control" required placeholder="" />
			</div>
			<button type="submit" class="btn btn-danger">Eliminar</button>
		</form>
	</div>
	<br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>



	<div id="tabla" style="opacity:0.9; padding:20px; border:3px solid gray;">
		<center><h1>Eliminar Administrador</h1></center>
		<table width="100%" >
			<tr>
				<td></td>
				<td>Nombre</td>
				<td>Apellidos</td>
				<td>Email</td>
				<td>Usuario</td>
				<td></td>

			</tr>
			<?php
				$temp= $_SESSION['primero']['Usuario'];
				//print_r($_SESSION['primero']['Usuario']);

				$consulta= "select * from usuarios where usuario != '".$temp."' ";
				$re= $conexion->query($consulta);
				//$resultado=$mysqli->query($query);
				while($row=$re ->fetch_assoc()){
					//print_r($row);
					echo '
					<tr>
						<td> <input type="hidden" class"Id" value="'.$row['Id'].'"> </td>
						
						<td>'.$row['Nombre'].'</td>
						<td>'.$row['Apellido'].'</td>
						<td>'.$row['correo'].'</td>
						<td>'.$row['Usuario'].'</td>
						<td>'.$row['Tipo'].'</td>
						<td> <input type="hidden" class"Id" value="'.$row['Password'].'"> </td>
					</tr>
					';
				}
			?>
		</table>
	</div>	
</body>
</html>

<?php 
}else{
		header("Location: ../login.php?Error=Acceso denegado");
	}
?>