<?php
session_start();
	include "../conexion.php";
	if(isset($_SESSION['primero'])){
?>
<html lang="es">
<head>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width,initial-scale=1.0">
    <title></title>
    <link rel="stylesheet" type="text/css" href="../css/style.css">

</head>
<body>

	<?php 
		include "menu.php";
	?>


	<section>

		<center><h1>Categorias existentes</h1></center>
		<div style="width: 30%; margin-left:20px; background:#CACACA; border-radius:20px; 
			margin-top:40px; margin-right:40px; padding:10px; float:left;">
				<form action="categorias/bajacategoria.php" method="POST">
					<div class="col-xs-6">
						<label>Categoria:</label>
						<input type="text" name="AdmiUser" class="form-control" required placeholder="" />
					</div>
					<button type="submit" class="btn btn-danger">Eliminar</button>
				</form>
			</div>
		<br><br>

		<div style="float:right;margin-right:185px;">
			<a href="#ventana1" class="btn btn-primary" data-toggle="modal">Agregar categoria</a>
			
			<!-- Inicia Vetana Modal -->
			<div class="modal fade" id="ventana1">
				<div class="modal-dialog">
					<div class="modal-content">
						<!-- Titulo -->
						<div class="modal-header">
							<button tyle="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
							<h2 class="modal-title">Nueva Categoria</h2>
						</div>
						
						<!-- Contenido -->
						<div class="modal-body">
							<form method="POST" action="categorias/altacategoria.php" enctype="multipart/form-data" id="Agregando">
								<div class="col-xs-8">
									<label>Nombre de la categoria:</label>
									<input type="text" name="nombre" class="form-control">
								</div>
								<br><br><br><br>							
						</div>

						<!-- Pie -->
						<div class="modal-footer">
							<button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
							<button type="submit" class="btn btn-success">Guardar</button>
							</form>
						</div>
					</div>
				</div>			
			</div>
			<!-- Termina Vetana Modal -->

		</div>
		<br><br><br><br><br>

		

	
		<?php

			/*$consulta= "select *from productos inner join Categorias on productos.categoria =
			Categorias.categoriaN where productos.categoria= '".$_GET['Name']."' AND productos.status=1 ";*/
			$si=mysqli_num_rows($re = $conexion->query("select *from categorias where status= 1 "));

			if($si==0){
		?>
				<h1>
					<i id="Nohay">
						<center>
							<img src="../imagenes/triste.jpg" id="carita" style="height:150px; padding:20px;">
							NO EXISTEN CATEGORIAS.
						</center>
					</i>
				</h1>
		<?php
			}else{
		?>
				<center>
					<div id="tabla" style="width:85%; opacity:0.9; padding:20px; border:3px solid gray;">

						<table class="table table-striped" width="80%" >
							<thead>
								<tr style="color:#144415;">
									<th></th>
									<th>Nombre de categoria</th>
									<th></th>

								</tr>
							</thead>
							<?php

								$consulta= "select * from categorias where status = 1 ";
								$re= $conexion->query($consulta);
								//$resultado=$mysqli->query($query);
								while($row=$re ->fetch_assoc()){
									//print_r($row);
									echo '
									<tr>
										<td> <input type="hidden" class"Id" value="'.$row['idCategoria'].'"> </td>
										
										<td>'.$row['categoriaN'].'</td>
									</tr>
									';
								}
							?>
						</table>
					</div>
				</center>
		<?php
				
			}
		?>
	</section>

</body>
</html>
<?php 
}else{
		header("Location: ../login.php?Error=Acceso denegado");
}
?>