<?php
session_start();
	include "../conexion.php";
	if(isset($_SESSION['primero'])){
?>
<html lang="es">
<head>
    <meta charset="utf-8"/>
    <title></title>
    <link rel="stylesheet" type="text/css" href="../css/style.css">

</head>
<body>

	<?php 
		include "menu.php";
	?>

	<section id="left" style="border-right: solid 4px rgba(218, 247, 207, 0.95);">
		<center><h1>Agregar mesa</h1></center>

		<div id="formulario" style="opacity:0.9; margin-top:50px;">
			<form method="POST" action="mesas/altamesas.php" enctype="multipart/form-data" id="Agregando">
				<div class="col-xs-8">
					<label>Imagen:</label>
					<input type="file" name="file">
				</div>
				<br><br><br><br>

				<div class="col-xs-8">
					<button type="submit" class="btn btn-success">Agregar</button>
				</div>
			</form>
		</div>
	</section>

	<section id="right">
		<center><h1>Mesas existentes</h1></center>
		<?php
			include '../conexion.php';

			/*$consulta= "select *from productos inner join Categorias on productos.categoria =
			Categorias.categoriaN where productos.categoria= '".$_GET['Name']."' AND productos.status=1 ";*/
			$si=mysqli_num_rows($re = $conexion->query("select *from mesas where status= 1 "));

			if($si==0){
		?>
				<h1>
					<i id="Nohay">
						<center>
							<img src="../imagenes/triste.jpg" id="carita" style="height:150px; padding:20px;">
							NO EXISTEN MESAS AGREGADAS.
						</center>
					</i>
				</h1>
		<?php
			}else{
				while ($f= mysqli_fetch_assoc($re)) {
		?>
				<div class="producto">
					<center>
						<img src="img/<?php echo $f['imagen'];?>" style="width: 40px;"><br>
						<span><?php echo $f['Id'];?></span><br>
					</center>
				</div>
		<?php
				}
			}
		?>
	</section>
	
</body>
</html>
<?php 
}else{
		header("Location: ../login.php?Error=Acceso denegado");
	}
?>