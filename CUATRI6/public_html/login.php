<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="utf-8"/>
	<meta name="viewport" content="width=device-width,initial-scale=1.0">
	<title></title>
	<link rel="stylesheet" type="text/css" href="css/style.css">
	<link rel="stylesheet" type="text/css" href="fonts.css">
	<link href="css/bootstrap.min.css" rel="stylesheet">
	<script type="text/javascript" src="http://code.jquery.com/jquery-1.10.2.min.js"></script>
	<script type="text/javascript"  href="./js/scripts.js"></script>
</head>
<body>
	
	<header>
		<img src="imagenes/logo.png" id="logo">
	</header>
	
	<section>
		<form id="formulario" method="post" action="acceder.php" style="opacity:0.9; margin-top:50px;">
			<h1>Login</h1>
			<?php 
			if(isset($_GET['error'])){
				echo '<center style="color:red;">Datos No Validos</center>';
			}
			?>
			<div class="col-xs-10">
				<label for="usuario">Usuario</label><br>
				<input type="text" id="Usuario" name="Usuario" class="form-control" placeholder="Ingrese su usuario" ><br>
			</div>
			<div class="col-xs-10">
				<label for="password">Password</label><br>
				<input type="password" id="Password" name="Password" class="form-control" placeholder="Ingrese su password" ><br>
			</div>
			<div class="col-xs-10">
				<button type="submit" class="btn btn-success">Acceder</button>
				<!--<input type="submit" name="aceptar" value="Aceptar" class="aceptar" style="margin:10px; width: 100px;">-->
			</div>
		</form>
	</section>

</body>
</html>