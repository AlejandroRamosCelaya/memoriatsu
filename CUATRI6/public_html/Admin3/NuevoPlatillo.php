<?php
session_start();
	include "../conexion.php";
	if(isset($_SESSION['tercero'])){
?>
<html lang="es">
<head>
    <meta charset="utf-8"/>
    <title></title>
    <link rel="stylesheet" type="text/css" href="../css/style.css">

</head>
<body>

	<?php 
		include "menu.php";
	?>


	<section>
			<div style="float:right;margin-right:185px;">
				<a href="#ventana1" class="btn btn-default active" data-toggle="modal">Agregar producto</a>
				<!-- Inicia Vetana Modal -->
				<div class="modal fade" id="ventana1">
					<div class="modal-dialog">
						<div class="modal-content">
							<!-- Titulo -->
							<div class="modal-header">
								<button tyle="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
								<h2 class="modal-title">Agregar Producto</h2>
							</div>
							
							<!-- Contenido -->
							<div class="modal-body">
								<form method="POST" action="platillo/altaplatillo.php" enctype="multipart/form-data" id="Agregando">
									<div class="col-xs-8">
										<label>Nombre del platillo:</label>
										<input type="text" name="nombre" class="form-control">
									</div>
									<br><br><br><br>

									<div class="col-xs-8">
										<label>Desscripción:</label>
										<input type="text" name="descripcion" class="form-control">
									</div>
									<br><br><br><br>

									<div class="col-xs-8">
										<label>Imagen:</label>
										<input type="file" name="file">
									</div>
									<br><br><br><br>

									<div class="col-xs-6">
										<label>Categoria:</label><br>
										<select type="text" name="categoria" class="form-control" required>
											<?php
											$consulta= "select * from categorias where Status = 1 ";
											$re= $conexion->query($consulta);
											while($row=$re ->fetch_assoc()){

												echo '<option>'.$row['categoriaN'].'</option>';
											}
											?>
										</select>
									</div>
									<br><br><br><br>

									<div class="col-xs-8">
										<label>Precio del platillo:</label>
										<input type="number" name="precio" class="form-control">
									</div>
									<br><br><br>

								
							</div>

							<!-- Pie -->
							<div class="modal-footer">
								<button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
								<button type="submit" class="btn btn-success">Guardar</button>
								</form>
							</div>
						</div>
					</div>			
				</div>
				<!-- Termina Vetana Modal -->

			</div>
			<br><br>
		<?php

			/*$consulta= "select *from productos inner join Categorias on productos.categoria =
			Categorias.categoriaN where productos.categoria= '".$_GET['Name']."' AND productos.status=1 ";*/
			$si=mysqli_num_rows($re = $conexion->query("select *from platillos where status= 1 "));

			if($si==0){
		?>
				<h1>
					<i id="Nohay">
						<center>
							<img src="../imagenes/triste.jpg" id="carita" style="height:150px; padding:20px;">
							NO HAY PRODUCTOS.
						</center>
					</i>
				</h1>
		<?php
			}else{
		?>

				<div id="tabla" style="opacity:0.9; padding:20px; border:3px solid gray;">
					<section id="left" style="border-right: solid 4px rgba(218, 247, 207, 0.95);">
						<div style="float:right;margin-right:185px;">
								<a href="#ventana2" class="btn btn-primary" data-toggle="modal">Desactivar producto</a>
								<!-- Inicia Vetana Modal -->
								<div class="modal fade" id="ventana2">
									<div class="modal-dialog">
										<div class="modal-content">
											<!-- Titulo -->
											<div class="modal-header">
												<button tyle="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
												<h2 class="modal-title">Desactivar producto</h2>
											</div>
											
											<!-- Contenido -->
											<div class="modal-body">
												<form action="platillo/bajaplatillo.php" method="POST">
													<div class="col-xs-6">
														<label>Identificador del producto:</label>
														<input type="number" name="id" class="form-control" />
													</div>
													<button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
													<button type="submit" class="btn btn-success">Desactivar</button>
												</form>
												<br><br>
											</div>
											<div class="modal-footer">
											</div>
										</div>
									</div>			
								</div>
								<!-- Termina Vetana Modal -->
						</div>	
					</section>

					<section id="right">
						<div style="float:right;margin-right:185px;">
							<a href="#ventana3" class="btn btn-primary" data-toggle="modal">Activar producto</a>
							<!-- Inicia Vetana Modal -->
							<div class="modal fade" id="ventana3">
								<div class="modal-dialog">
									<div class="modal-content">
										<!-- Titulo -->
										<div class="modal-header">
											<button tyle="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
											<h2 class="modal-title">Activar producto</h2>
										</div>
										
										<!-- Contenido -->
										<div class="modal-body">
											<form action="platillo/activaplatillo.php" method="POST">
												<div class="col-xs-6">
													<label>Identificador del producto:</label>
													<input type="number" name="id" class="form-control" />
												</div>
												<button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
												<button type="submit" class="btn btn-success">Activar</button>
											</form>
											<br><br>
										</div>
										<div class="modal-footer">
										</div>
									</div>
								</div>			
							</div>
							<!-- Termina Vetana Modal -->
						</div>
					</section>
					
					<table width="100%" >
						<tr>
							<td style="color:#0070ff;">Identificador</td>
							<td style="color:#0070ff;">Categoria</td>
							<td style="color:#0070ff;">Nombre</td>
							<td style="color:#0070ff;">Precio</td>
							<td style="color:#0070ff;">Imagen</td>
							<td style="color:#0070ff;">Status</td>

						</tr>
						<?php
							$consulta= "select *from platillos";
							$re= $conexion->query($consulta);
							//$resultado=$mysqli->query($query);
							while($row=$re ->fetch_assoc()){
								if ($row['status']==1) {
									echo '
									<tr>
										<td style="color:red;"> '.$row['id'].' </td>
										<td>'.$row['Categoria'].'</td>
										<td>'.$row['nombre'].'</td>
										<td>$ '.$row['precio'].'</td>
										<td style="width:150px;">
											<img src="../img/'.$row['imagen'].'" 
											width="60px" heigth="60px"/>
										</td>

										<td>Activo</td>
									</tr>
									';
								}elseif ($row['status']==0) {
									echo '
									<tr>
										<td style="color:red;"> '.$row['id'].' </td>
										<td>'.$row['Categoria'].'</td>
										<td>'.$row['nombre'].'</td>
										<td>$ '.$row['precio'].'</td>
										<td style="width:150px;">
											<img src="../img/'.$row['imagen'].'" 
											width="60px" heigth="60px"/>
										</td>

										<td>Inactivo</td>
									</tr>
									';
								}
								
							}
						?>
					</table>
				</div>
		<?php
				
			}
		?>
	</section>
	
</body>
</html>
<?php 
}else{
		header("Location: ../login.php?Error=Acceso denegado");
}
?>